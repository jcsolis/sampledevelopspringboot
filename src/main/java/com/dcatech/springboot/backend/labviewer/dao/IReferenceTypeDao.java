package com.dcatech.springboot.backend.labviewer.dao;

import com.dcatech.springboot.backend.labviewer.entity.ReferenceType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IReferenceTypeDao extends CrudRepository<ReferenceType, String> {
    public ReferenceType findByRefTypeId(String refTypeId);
}
