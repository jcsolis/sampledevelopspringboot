package com.dcatech.springboot.backend.labviewer.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.EnableMBeanExport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "U_MENUOPTION")
public class LvMenu implements Serializable {

    @Id
    @Column(name = "U_MENUOPTIONID")
    private String menuOptionId;

    @Column(name = "MENUOPTIONDESC")
    private String menuOptionDesc;

    @Column(name = "LABELMENU")
    private String menuOptionLabel;

    @Column(name = "ICONMENU")
    private String menuIconName;

    @Column(name = "SECURITYSET")
    private String menuSecuritySet;

    @Column(name = "ORDERMENU")
    private Long menuOrder;


    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "MENUOPTIONID")
    @OrderBy(value = "ORDERITEM")
    private List<LvMenuItem> submenu;


    private static final long serialVersionUID = 1342900979573570454L;

    public String getMenuOptionId() {
        return menuOptionId;
    }

    public String getMenuOptionDesc() {
        return menuOptionDesc;
    }

    public String getMenuOptionLabel() {
        return menuOptionLabel;
    }

    public String getMenuIconName() {
        return menuIconName;
    }

    public String getMenuSecuritySet() {
        return menuSecuritySet;
    }

    public Long getMenuOrder() {
        return menuOrder;
    }

    public List<LvMenuItem> getSubmenu() {
        return submenu;
    }
}
