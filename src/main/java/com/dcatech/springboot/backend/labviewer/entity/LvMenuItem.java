package com.dcatech.springboot.backend.labviewer.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "U_MENUITEM")
public class LvMenuItem implements Serializable {

    private static final long serialVersionUID = -1745676881928085555L;

    @Id
    @Column(name = "U_MENUITEMID")
    private String menuItemId;

    @Column(name = "MENUOPTIONID")
    private String menuOptionId;

    @Column(name = "ORDERITEM")
    private String menuItemOrder;

    @Column(name = "ICONITEM")
    private String menuItemIcon;

    @Column(name = "QUERYID")
    private String menuItemQueryId;

    @Column(name = "SECURITYSET")
    private String menuItemSecuritySet;

    @Column(name = "TYPEITEM")
    private String menuItemType;

    @Column(name = "CLICK_COUNT")
    private Long menuItemCount;

    @Column(name = "LABELITEM")
    private String menuItemLabel;

    @Column(name = "VISIBLE")
    private String menuItemStatus;

    public String getMenuItemId() {
        return menuItemId;
    }

    public String getMenuOptionId() {
        return menuOptionId;
    }

    public String getMenuItemOrder() {
        return menuItemOrder;
    }

    public String getMenuItemIcon() {
        return menuItemIcon;
    }

    public String getMenuItemQueryId() {
        return menuItemQueryId;
    }

    public String getMenuItemSecuritySet() {
        return menuItemSecuritySet;
    }

    public String getMenuItemType() {
        return menuItemType;
    }

    public Long getMenuItemCount() {
        return menuItemCount;
    }

    public String getMenuItemLabel() {
        return menuItemLabel;
    }

    public String getMenuItemStatus() {
        return menuItemStatus;
    }
}
