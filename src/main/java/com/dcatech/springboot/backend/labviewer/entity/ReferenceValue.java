package com.dcatech.springboot.backend.labviewer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "REFVALUE")

public class ReferenceValue implements Serializable {

    private static final long serialVersionUID = 1650695409473360841L;

    @Id
    @Column(name = "REFTYPEID")
    private String refTypeId;

    @Column(name = "REFVALUEID")
    private String refValueId;

    @Column(name = "REFVALUEDESC")
    private String refValueDesc;

    @Column(name = "REFDISPLAYVALUE")
    private String refDisplayValue;

    public String getRefTypeId() {
        return refTypeId;
    }

    public String getRefValueId() {
        return refValueId;
    }

    public String getRefValueDesc() {
        return refValueDesc;
    }

    public String getRefDisplayValue() {
        return refDisplayValue;
    }
}
