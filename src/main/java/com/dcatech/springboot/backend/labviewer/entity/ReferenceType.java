package com.dcatech.springboot.backend.labviewer.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "REFTYPE")
public class ReferenceType implements Serializable {

    private static final long serialVersionUID = -6342119877309666434L;


    @Id
    @Column(name = "REFTYPEID")
    private String refTypeId;

    @Column(name = "REFTYPEDESC")
    private String refTypeDesc;

    @Column(name = "STATICFLAG")
    private String staticFlag;

    @Column(name = "TYPEFLAG")
    private String typeFlag;

    @Column(name = "DISPLAYVALUESFLAG")
    private String displayValuesFlag;

    @Column(name = "MAXLENGTH")
    private Long maxLength;

    @Column(name = "USERSEQUENCE")
    private Long userSequence;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "CREATEDT")
    private Date createDt;

    @Column(name = "CREATEBY")
    private String createBy;

    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "REFTYPEID")
    private List<ReferenceValue> refValues;

    public String getRefTypeId() {
        return refTypeId;
    }

    public String getRefTypeDesc() {
        return refTypeDesc;
    }

    public String getStaticFlag() {
        return staticFlag;
    }

    public String getTypeFlag() {
        return typeFlag;
    }

    public String getDisplayValuesFlag() {
        return displayValuesFlag;
    }

    public Long getMaxLength() {
        return maxLength;
    }

    public Long getUserSequence() {
        return userSequence;
    }

    public String getNotes() {
        return notes;
    }

    public Date getCreateDt() {
        return createDt;
    }

    public String getCreateBy() {
        return createBy;
    }

    public List<ReferenceValue> getRefValues() {
        return refValues;
    }
}
