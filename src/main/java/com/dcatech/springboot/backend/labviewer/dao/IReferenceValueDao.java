package com.dcatech.springboot.backend.labviewer.dao;

import com.dcatech.springboot.backend.labviewer.entity.ReferenceValue;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IReferenceValueDao extends CrudRepository<ReferenceValue, String> {
    public List<ReferenceValue>findByRefTypeId(String refType);
}
