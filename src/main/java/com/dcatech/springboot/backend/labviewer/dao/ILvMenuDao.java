package com.dcatech.springboot.backend.labviewer.dao;

import com.dcatech.springboot.backend.labviewer.entity.LvMenu;
import org.springframework.data.repository.CrudRepository;


import java.util.List;


public interface ILvMenuDao extends CrudRepository<LvMenu, String> {

    public List<LvMenu> findByOrderByMenuOrder();
}
