package com.dcatech.springboot.backend.labviewer.services;

import com.dcatech.springboot.backend.labviewer.entity.LvMenu;
import com.dcatech.springboot.backend.labviewer.entity.ReferenceType;
import com.dcatech.springboot.backend.labviewer.entity.ReferenceValue;

import java.util.List;

public interface IQueryService {


    public List<ReferenceType> findAll();

    public List<LvMenu> findAllLvMenu();

    public List<ReferenceValue> findByRefTypeId(String refTypeId);

    public ReferenceType findByRefTypeIdRefType(String refTypeId);

}
