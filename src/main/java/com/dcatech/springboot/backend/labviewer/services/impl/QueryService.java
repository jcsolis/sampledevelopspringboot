package com.dcatech.springboot.backend.labviewer.services.impl;

import com.dcatech.springboot.backend.labviewer.dao.ILvMenuDao;
import com.dcatech.springboot.backend.labviewer.dao.IReferenceTypeDao;
import com.dcatech.springboot.backend.labviewer.dao.IReferenceValueDao;
import com.dcatech.springboot.backend.labviewer.entity.LvMenu;
import com.dcatech.springboot.backend.labviewer.entity.ReferenceType;
import com.dcatech.springboot.backend.labviewer.entity.ReferenceValue;
import com.dcatech.springboot.backend.labviewer.services.IQueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
public class QueryService implements IQueryService {

    @Autowired
    private IReferenceTypeDao referenceTypeDao;

    @Autowired
    private ILvMenuDao lvMenuDao;

    @Autowired
    private IReferenceValueDao referenceValueDao;

    private Logger logger = LoggerFactory.getLogger(QueryService.class);


    @Override
    @Transactional(readOnly = true)
    public List<ReferenceType> findAll() {
        return (List<ReferenceType>) referenceTypeDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<LvMenu> findAllLvMenu() {

        return lvMenuDao.findByOrderByMenuOrder();
    }

    @Override
    @Transactional(readOnly = true)
    public List<ReferenceValue> findByRefTypeId(String refTypeId) {
        logger.error("Datos enviado " + refTypeId );
        return referenceValueDao.findByRefTypeId(refTypeId);
    }

    @Override
    @Transactional(readOnly = true)
    public ReferenceType findByRefTypeIdRefType(String refTypeId) {
        return referenceTypeDao.findByRefTypeId(refTypeId);
    }
}
