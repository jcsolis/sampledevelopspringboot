package com.dcatech.springboot.backend.labviewer.controllers;


import com.dcatech.springboot.backend.labviewer.entity.LvMenu;
import com.dcatech.springboot.backend.labviewer.entity.ReferenceType;
import com.dcatech.springboot.backend.labviewer.entity.ReferenceValue;
import com.dcatech.springboot.backend.labviewer.services.IQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.PreparedStatement;
import java.util.List;

@RestController
@RequestMapping("/query")
public class QueryRestController {

    @Autowired
    private IQueryService queryService;

    @GetMapping("/referenceType")
    public List<ReferenceType> index(){

        return queryService.findAll();

    }


    @GetMapping("/menu")
    public List<LvMenu> getMenuOption(){
        return queryService.findAllLvMenu();
    }

    @GetMapping("/referenceValue/{refTypeId}")
    public List<ReferenceValue> getRefTypeValue(@PathVariable String refTypeId){
        return queryService.findByRefTypeId(refTypeId);
    }

    @GetMapping("/refType/{refTypeId}")
    public ReferenceType getRefType(@PathVariable String refTypeId){
        return queryService.findByRefTypeIdRefType(refTypeId);
    }

}
