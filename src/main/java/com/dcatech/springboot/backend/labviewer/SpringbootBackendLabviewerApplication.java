package com.dcatech.springboot.backend.labviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBackendLabviewerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackendLabviewerApplication.class, args);
	}

}
